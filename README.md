# python data structures

Just some implementations of basic data structures in python.

## Singly Linked List

Files::

    linkedlist_naive.py

        A "naive" singly linked list class.

    test_linkedlist_naive.py

        Test code for the naive linked list.

    linkedlist.py

        A rework of linkedlist_naive.py with some enhancements.

    test_linkedlist.py

        Test code for the enhanced linked list.

    speed_test.py

        ????

    hashtable_naive.py 

        A "naive" hash table implementation.

    test_hashtable_naive.py 

        Test code for the naive hash table.
