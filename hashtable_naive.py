"""
A module that implements a hashtable with chaining in the case of a collision.

This naive initial implementation has no speedup code.
"""


class HashTable:
    """Implement a hashtable."""

    class Bucket:
        """A 'chaining' bucket in the hashtable."""

        def __init__(self, value, next=None):
            self.value = value
            self.next = next


    def __init__(self, size=2**10, hashfun=None):
        """Create a hashtable of the given size."""

        self.size = size
        self.hashtable = [None for _ in range(size)]
        self.hash_function = self.hash
        if hashfun:
            self.hash_function = hashfun
        self.bitmask = self.size - 1

    def hash(self, obj):
        """Get a hash value for the given object."""

        return hash(obj)

    def insert(self, obj):
        """Add new object 'obj' to the hashtable."""

        hashval = self.hash_function(obj) & self.bitmask
        self.hashtable[hashval] = HashTable.Bucket(obj, self.hashtable[hashval])

    def lookup(self, obj):
        """Finds 'obj' in the hashtable and returns a reference to it.

        Returns None if not found.
        """

        hashval = self.hash_function(obj) & self.bitmask
        scan = self.hashtable[hashval]
        while scan:
            if scan.value is obj:
                return obj
            scan = scan.next

    def _info(self):
        """Return a string describing the state of the hashtable."""

        entry_count = 0
        bucket_count = 0
        body = []
        for (i, bc) in enumerate(self.hashtable):
            if bc is not None:
                line = []
                entry_count += 1
                bucket_count += 1
                scan = bc
                line.append(f'{scan.value}')
                while scan.next:
                    line.append(f'{scan.value}')
                    bucket_count += 1
                    scan = scan.next
                body.append(f'Hash index {i}: ' + ', '.join(line))
        body.insert(0, f'HashTable: entries={entry_count}, buckets={bucket_count}')
        return '\n'.join(body)

    def __str__(self):
        """String representation of the hashtable."""

        return f'<HashTable: size {self.size}>'
