"""
A module that implements a singly linked list class.

Some speedups to get efficiency:
    * keep 'last' pointer to speed up .append()
    * keep length count to speedup .__len__()
"""


class LinkedList:
    """Implement a singly-linked list."""

    class Node:
        """A node in the linked list."""

        def __init__(self, value, next=None):
            self.value = value
            self.next = next


    def __init__(self, values=None):
        """Create an instance, with optional initial values."""

        self.head = None
        self.last = None        # reference to last Node in the list
        self.len = 0            # keep count of elements in list

        if values:
            for v in values:
                self.append(v)

    def append(self, value):
        """Add new value to the *end* of the linked list."""

        new = LinkedList.Node(value)

        if self.last:
            # non-empty list, easy
            self.last.next = new
            self.last = new
        else:
            # empty list
            self.head = self.last = new

        self.len += 1

    def push(self, value):
        """Add new value to the *start* of the linked list."""

        self.head = LinkedList.Node(value, self.head)
        self.len += 1

    def insert_after(self, old, new):
        """Insert value "new" after element with value "old"."""

        if self.head:
            if self.head.value == old:
                # first Node, insert after
                if self.head == self.last:
                    self.last = self.head.next = LinkedList.Node(new, self.head.next)
                else:
                    self.head.next = LinkedList.Node(new, self.head.next)
                self.len += 1
            else:
                scan = self.head
                while scan != None:
                    if scan.value == old:
                        # insert "new" Node after this one
                        if scan == self.last:
                            scan.next = self.last = LinkedList.Node(new, scan.next)
                        else:
                            scan.next = LinkedList.Node(new, scan.next)
                        self.len += 1
                        break
                    scan = scan.next

    def insert_sorted(self, value, key=None):
        """Insert 'value' in to the correct place in a sorted list.

        key  address of 'key()' function to compare values (optional)

        Default sort is increasing values from list head.
        """

        # if no 'key' given, assume default key function
        if key is None:
            key = lambda v1, v2: v1 > v2

        if self.head:
            # non-empty list
            scan = self.head

            # check if we need to insert at head of list
            if key(scan.value, value):
                self.head = LinkedList.Node(value, self.head)
                self.len += 1
                return

            while scan:
                if scan.next and key(scan.next.value, value):
                    scan.next = LinkedList.Node(value, scan.next)
                    self.len += 1
                    return
                scan = scan.next
            # off the end, put new value on end of list
            self.append(value)
        else:
            # empty list
            self.head = self.last = LinkedList.Node(value)
            self.len = 1

    def delete(self, value):
        """Delete node in list with 'value'."""

        if self.head is not None:
            if self.head.value == value:
                # first Node is to be removed
                self.head = self.head.next
                self.len -= 1
            else:
                scan = self.head
                while scan != None:
                    if scan.next is not None and scan.next.value == value:
                        # Node *before* Node containing 'value'
                        scan.next = scan.next.next
                        self.len -= 1
                        break
                    scan = scan.next


    def __len__(self):
        """Get length of the list."""

        return self.len

    def __getitem__(self, position):
        """Return the value of item at 'position'."""

        # handle negative indices
        if position < 0:
            position = len(self) - abs(position)

        index = 0
        scan = self.head
        while scan:
            if index == position:
                return scan.value
            index += 1
            scan = scan.next

        raise IndexError

    def __str__(self):
        """Draw contents of the linked list."""

        result = []

        scan = self.head
        while scan:
            result.append(str(scan.value))
            scan = scan.next
        return '->'.join(result)
