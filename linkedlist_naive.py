"""
A module that implements a singly linked list class.

This naive initial implementation has no speedup code.
"""


class LinkedList:
    """Implement a singly-linked list."""

    class Node:
        """A node in the linked list."""

        def __init__(self, value, next=None):
            self.value = value
            self.next = next


    def __init__(self, values=None):
        """Create an instance, with optional initial values."""

        self.head = None

        if values:
            for v in values:
                self.append(v)

    def append(self, value):
        """Add new value to the *end* of the linked list."""

        if self.head:
            # list not empty, find last Node
            scan = self.head
            while scan.next:
                scan = scan.next
            scan.next = LinkedList.Node(value)
        else:
            # list is empty, just insert new Node
            self.head = LinkedList.Node(value)

    def push(self, value):
        """Add new value to the *start* of the linked list."""

        self.head = LinkedList.Node(value, self.head)

    def insert_after(self, old, new):
        """Insert value "new" after element with value "old"."""

        if self.head is not None:
            if self.head.value == old:
                # first Node, insert after
                self.head.next = LinkedList.Node(new, self.head.next)
            else:
                scan = self.head
                while scan != None:
                    if scan.value == old:
                        # insert "new" Node after this one
                        scan.next = LinkedList.Node(new, scan.next)
                        break
                    scan = scan.next

    def delete(self, value):
        """Delete node in list with 'value'."""

        if self.head is not None:
            if self.head.value == value:
                # first Node is to be removed
                self.head = self.head.next
            else:
                scan = self.head
                while scan != None:
                    if scan.next is not None and scan.next.value == value:
                        # Node *before* Node containing 'value'
                        scan.next = scan.next.next
                        break
                    scan = scan.next

    def __len__(self):
        """Get length of the list."""

        length = 0
        scan = self.head
        while scan:
            length += 1
            scan = scan.next
        return length

    def __getitem__(self, position):
        """Return the value of item at 'position'."""

        # handle negative indices
        if position < 0:
            position = len(self) - abs(position)

        index = 0
        scan = self.head
        while scan:
            if index == position:
                return scan.value
            index += 1
            scan = scan.next

        raise IndexError

    def __str__(self):
        """Draw contents of the linked list."""

        result = []

        scan = self.head
        while scan:
            result.append(str(scan.value))
            scan = scan.next
        return '->'.join(result)
