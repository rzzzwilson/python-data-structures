"""
Test the HashTable class.
"""

import hashtable_naive as ht
import unittest

class TestHashTable(unittest.TestCase):

    def test_empty(self):
        """Just instantiate an empty hashtable."""

        htable = ht.HashTable()

    def test_insert(self):
        """Try inserting items into an empty hashtable."""

        htable = ht.HashTable()
        htable.insert(1)
        htable.insert(2)
        htable.insert(1)

    def test_lookup(self):
        """Try finding an object in the hashtable."""

        htable = ht.HashTable()
        htable.insert(1)
        htable.insert(2)
        htable.insert(1)
        value = htable.lookup(1)
        expected = 1
        msg = f'lookup(1) returned {value}, expected {expected}'
        self.assertEqual(value, expected, msg)

    def test_hash_function(self):
        """Try a user-supplied hash function./"""

        def hf (obj):
            """Hash a (key, value) tuple."""

            return hash(obj[0])

        htable = ht.HashTable(hashfun=hf)
        htable.insert((1, 'alpha'))
        htable.insert((2, 'beta'))
        htable.insert((1, 'gamma'))
        print('\n' + htable._info())
        value = htable.lookup((1, 'gamma'))
        expected = (1, 'gamma')
        msg = f"lookup((1, 'gamma')) returned {value}, expected {expected}"
        self.assertEqual(value, expected, msg)


unittest.main()
