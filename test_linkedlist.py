"""
Test the singly linked list class.
"""

import linkedlist as ll
import unittest

class TestLinkedList(unittest.TestCase):

    def test_empty(self):
        """Just instantiate an empty list."""

        l = ll.LinkedList()
        self.assertEqual(len(l), 0)             # length should be zero

    def test_append(self):
        """Try appending to an empty list."""

        l = ll.LinkedList()
        l.append(1)
        self.assertEqual(len(l), 1)             # length should be 1
        l.append(2)
        self.assertEqual(len(l), 2)             # length should be 2
        value = str(l)
        expected = '1->2'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents

    def test_initial_values(self):
        """Create a linked list with initial values."""

        l = ll.LinkedList([1, 2, 3])
        value = str(l)
        expected = '1->2->3'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents

    def test_push(self):
        """Try pushing to a list."""

        l = ll.LinkedList()
        l.push(1)
        self.assertEqual(len(l), 1)             # length should be 1
        l.push(2)
        self.assertEqual(len(l), 2)             # length should be 2
        value = str(l)
        expected = '2->1'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents

    def test_insert_after(self):
        """Try insert_after()."""

        l = ll.LinkedList()
        l.append(1)
        self.assertEqual(len(l), 1)             # length should be 1
        l.append(2)
        self.assertEqual(len(l), 2)             # length should be 2
        l.append(3)
        self.assertEqual(len(l), 3)             # length should be 3
        value = str(l)
        expected = '1->2->3'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents

        # insert after first head element
        l.insert_after(1, 4)
        self.assertEqual(len(l), 4)             # length should be 4
        value = str(l)
        expected = '1->4->2->3'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents

        # insert after element in middle
        l.insert_after(4, 5)
        self.assertEqual(len(l), 5)             # length should be 5
        value = str(l)
        expected = '1->4->5->2->3'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents

        # insert after element at end
        l.insert_after(3, 6)
        self.assertEqual(len(l), 6)             # length should be 6
        value = str(l)
        expected = '1->4->5->2->3->6'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents

        # insert but no such element
        l.insert_after(999, 7)
        self.assertEqual(len(l), 6)             # length should still be 6
        value = str(l)
        expected = '1->4->5->2->3->6'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents

    def test_append_push(self):
        """Try mixed appending and pushing to a list."""

        l = ll.LinkedList()
        l.append(1)
        self.assertEqual(len(l), 1)             # length should be 1
        l.push(2)
        self.assertEqual(len(l), 2)             # length should be 2
        l.append(3)
        self.assertEqual(len(l), 3)             # length should be 3
        value = str(l)
        expected = '2->1->3'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents

    def test_delete(self):
        """Try deleting element in a list."""

        l = ll.LinkedList()
        l.append(1)
        self.assertEqual(len(l), 1)             # length should be 1
        l.append(2)
        self.assertEqual(len(l), 2)             # length should be 2
        l.append(3)
        self.assertEqual(len(l), 3)             # length should be 3
        l.append(4)
        self.assertEqual(len(l), 4)             # length should be 4
        l.append(5)
        self.assertEqual(len(l), 5)             # length should be 5

        # delete element at front of list
        l.delete(1)
        self.assertEqual(len(l), 4)
        value = str(l)
        expected = '2->3->4->5'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents

        # delete element at end of list
        l.delete(5)
        self.assertEqual(len(l), 3)
        value = str(l)
        expected = '2->3->4'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents

        # delete element in middle of list
        l.delete(3)
        self.assertEqual(len(l), 2)
        value = str(l)
        expected = '2->4'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents

        # delete element that doesn't exist
        l.delete(99)
        self.assertEqual(len(l), 2)
        value = str(l)
        expected = '2->4'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents

    def test_indexing(self):
        """Try indexing into a list."""

        # test index of start of list
        l = ll.LinkedList()
        l.append(1)
        self.assertEqual(len(l), 1)             # length should be 1
        l.append(2)
        self.assertEqual(len(l), 2)             # length should be 2
        l.append(3)
        self.assertEqual(len(l), 3)             # length should be 3
        value = l[0]
        expected = 1
        msg = f"list={str(l)}, l[1] should be {expected}, got {value}"
        self.assertEqual(value, expected, msg)   # check returned value

        # test index into middle
        value = l[1]
        expected = 2
        msg = f"list={str(l)}, l[1] should be {expected}, got {value}"
        self.assertEqual(value, expected, msg)   # check returned value

        # test positive index out of range
        msg = f"list={str(l)}, l[99] should raise 'IndexError', didn't"
        with self.assertRaises(IndexError, msg=msg):
            l[99]

        # test negative index
        value = l[-1]
        expected = 3
        msg = f"list={str(l)}, l[-1] should be {expected}, got {value}"
        self.assertEqual(value, expected, msg)   # check returned value

        # test negative index out of rage
        msg = f"list={str(l)}, l[-7] should raise 'IndexError', didn't"
        with self.assertRaises(IndexError, msg=msg):
            l[-7]

    def test_sorted(self):
        """Try inserting into a sorted list."""

        # test insert_sorted into an empty list
        l = ll.LinkedList()
        l.insert_sorted(5)
        value = str(l)
        expected = '5'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents

        # create short sorted list
        l = ll.LinkedList()
        l.append(2)
        self.assertEqual(len(l), 1)             # length should be 1
        l.append(4)
        self.assertEqual(len(l), 2)             # length should be 2
        value = str(l)
        expected = '2->4'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents

        # insert value that sorts to front of list
        l.insert_sorted(0)
        value = str(l)
        expected = '0->2->4'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents

        # insert value in middle of sorted list
        l.insert_sorted(3)
        value = str(l)
        expected = '0->2->3->4'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents

        # insert value at end of sorted list
        l.insert_sorted(5)
        value = str(l)
        expected = '0->2->3->4->5'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents

    def test_sorted_key(self):
        """Try inserting into a sorted list using a key function."""

        def key(v1, v2):
            return v1 <= v2

        # test insert_sorted into an empty list
        l = ll.LinkedList()
        l.insert_sorted(5, key=key)
        value = str(l)
        expected = '5'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents
        self.assertEqual(len(l), 1)             # length should be 3

        # create short sorted list
        l = ll.LinkedList()
        l.append(4)
        l.append(2)
        self.assertEqual(len(l), 2)             # length should be 2
        value = str(l)
        expected = '4->2'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents

        # insert value that sorts to front of list
        l.insert_sorted(6, key=key)
        value = str(l)
        expected = '6->4->2'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents
        self.assertEqual(len(l), 3)             # length should be 3

        # insert value in middle of sorted list
        l.insert_sorted(3, key=key)
        value = str(l)
        expected = '6->4->3->2'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents
        self.assertEqual(len(l), 4)             # length should be 4

        # insert value at end of sorted list
        l.insert_sorted(0, key=key)
        value = str(l)
        expected = '6->4->3->2->0'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents
        self.assertEqual(len(l), 5)             # length should be 5

unittest.main()
