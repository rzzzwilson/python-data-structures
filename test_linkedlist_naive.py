"""
Test the naive implementation of the LinkedList class.
"""


import unittest
import linkedlist_naive as ll


class TestLinkedList(unittest.TestCase):

    def test_empty(self):
        """Just instantiate an empty list."""

        l = ll.LinkedList()
        self.assertEqual(len(l), 0)             # length should be zero

    def test_append(self):
        """Try appending to an empty list."""

        l = ll.LinkedList()
        l.append(1)
        self.assertEqual(len(l), 1)             # length should be 1
        l.append(2)
        self.assertEqual(len(l), 2)             # length should be 2
        value = str(l)
        expected = '1->2'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents

    def test_initial_values(self):
        """Create a linked list with initial values."""

        l = ll.LinkedList([1, 2, 3])
        value = str(l)
        expected = '1->2->3'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents

    def test_push(self):
        """Try pushing to a list."""

        l = ll.LinkedList()
        l.push(1)
        self.assertEqual(len(l), 1)             # length should be 1
        l.push(2)
        self.assertEqual(len(l), 2)             # length should be 2
        value = str(l)
        expected = '2->1'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents

    def test_insert_after(self):
        """Try insert_after()."""

        l = ll.LinkedList()
        l.append(1)
        self.assertEqual(len(l), 1)             # length should be 1
        l.append(2)
        self.assertEqual(len(l), 2)             # length should be 2
        l.append(3)
        self.assertEqual(len(l), 3)             # length should be 3
        value = str(l)
        expected = '1->2->3'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents

        # insert after first head element
        l.insert_after(1, 4)
        self.assertEqual(len(l), 4)             # length should be 4
        value = str(l)
        expected = '1->4->2->3'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents

        # insert after element in middle
        l.insert_after(4, 5)
        self.assertEqual(len(l), 5)             # length should be 5
        value = str(l)
        expected = '1->4->5->2->3'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents

        # insert after element at end
        l.insert_after(3, 6)
        self.assertEqual(len(l), 6)             # length should be 6
        value = str(l)
        expected = '1->4->5->2->3->6'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents

        # insert but no such element
        l.insert_after(999, 7)
        self.assertEqual(len(l), 6)             # length should still be 6
        value = str(l)
        expected = '1->4->5->2->3->6'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents

    def test_append_push(self):
        """Try mixed appending and pushing to a list."""

        l = ll.LinkedList()
        l.append(1)
        self.assertEqual(len(l), 1)             # length should be 1
        l.push(2)
        self.assertEqual(len(l), 2)             # length should be 2
        l.append(3)
        self.assertEqual(len(l), 3)             # length should be 3
        value = str(l)
        expected = '2->1->3'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents

    def test_delete(self):
        """Try deleting element in a list."""

        l = ll.LinkedList()
        l.append(1)
        self.assertEqual(len(l), 1)             # length should be 1
        l.append(2)
        self.assertEqual(len(l), 2)             # length should be 2
        l.append(3)
        self.assertEqual(len(l), 3)             # length should be 3
        l.append(4)
        self.assertEqual(len(l), 4)             # length should be 4
        l.append(5)
        self.assertEqual(len(l), 5)             # length should be 5

        # delete element at front of list
        l.delete(1)
        self.assertEqual(len(l), 4)
        value = str(l)
        expected = '2->3->4->5'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents

        # delete element at end of list
        l.delete(5)
        self.assertEqual(len(l), 3)
        value = str(l)
        expected = '2->3->4'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents

        # delete element in middle of list
        l.delete(3)
        self.assertEqual(len(l), 2)
        value = str(l)
        expected = '2->4'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents

        # delete element that doesn't exist
        l.delete(99)
        self.assertEqual(len(l), 2)
        value = str(l)
        expected = '2->4'
        msg = f"Expected contents '{expected}', got '{value}'"
        self.assertEqual(value, expected, msg)  # check contents

    def test_indexing(self):
        """Try indexing into a list."""

        # test index of start of list
        l = ll.LinkedList()
        l.append(1)
        self.assertEqual(len(l), 1)             # length should be 1
        l.append(2)
        self.assertEqual(len(l), 2)             # length should be 2
        l.append(3)
        self.assertEqual(len(l), 3)             # length should be 3
        value = l[0]
        expected = 1
        msg = f"list={str(l)}, l[1] should be {expected}, got {value}"
        self.assertEqual(value, expected, msg)   # check returned value

        # test index into middle
        value = l[1]
        expected = 2
        msg = f"list={str(l)}, l[1] should be {expected}, got {value}"
        self.assertEqual(value, expected, msg)   # check returned value

        # test negative index
        value = l[-1]
        expected = 3
        msg = f"list={str(l)}, l[-1] should be {expected}, got {value}"
        self.assertEqual(value, expected, msg)   # check returned value

        # test negative index out of rage
        msg = f"list={str(l)}, l[-7] should raise 'IndexError', didn't"
        with self.assertRaises(IndexError, msg=msg):
            l[-7]


unittest.main()
