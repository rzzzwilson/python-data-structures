"""
Code to test the speed of the linked list classes.
"""

import sys
import time
import linkedlist as ll
import linkedlist_naive as lln

Size = 10_000

print(f'Size of data list: {Size:,}\n')

#####
# Initialization
#####

# first, do naive linked list
print('   Naive: initialization took ', end='')
sys.stdout.flush()

data = range(Size)
start = time.time()
l = lln.LinkedList(data)
delta = time.time() - start

print(f'{delta:5.2f}s')

# now the improved linked list
print('Improved: initialization took ', end='')
sys.stdout.flush()

data = range(Size)
start = time.time()
l = ll.LinkedList(data)
delta = time.time() - start

print(f'{delta:5.2f}s\n')

#####
# Append
#####

# naive linked list
print('   Naive: append took ', end='')
sys.stdout.flush()

start = time.time()
l = lln.LinkedList()
for val in range(Size):
    l.append(val)
delta = time.time() - start

print(f'{delta:5.2f}s')

# improved linked list
print('Improved: append took ', end='')
sys.stdout.flush()

data = range(Size)
start = time.time()
l = ll.LinkedList()
for val in range(Size):
    l.append(val)
delta = time.time() - start

print(f'{delta:5.2f}s\n')

print()
